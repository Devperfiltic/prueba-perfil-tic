<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

Este proyecto contiene la estructura inicial del backend, el cual contiene toda las herramientas necesarias para crear un proyecto.

# Instalación :

Debemos importar todo los paquetes (Composer) que son necesarios para el proyecto.

```sh
composer install
```

Debemos modificar las variables de configuración del
proyecto, estas variables son por ejemplo nombre de la
base de datos, nombre del proyecto, etc. Esto se hace 
modificando el archivo .env en la raíz del proyecto. 
Luego de dicha modificación del archivo debemos generar 
nuevas llave de seguridad para nuestro proyecto, 
esto se hace mediante los siguientes comandando.

```sh
php artisan jwt:secret
php artisan key:generate 
php artisan config:cache
```

Debemos instalar Redis para el manejo de  trabajos en cola y como 
un componente  importante para las notificaciones en tiempo real.

Instalacion para ubuntu:
```sh
sudo apt-get update
sudo apt-get install redis-server
```

Debemos  ejecutar un trabajador  en segundo plano el cual nos permitirá
realizar  las tareas que se encuentran en pila. 

 - Nota : En modo despliegue es mejor usar un supervisor 
 revisar la documentacion oficial de Laravel.
 https://laravel.com/docs/5.8/queues#supervisor-configuration
 
```sh
php artisan  queue:work
```
Ahora podemos correr nuestro proyecto backend de forma local.

```sh
php artisan serve
```