<?php

namespace App\Http\Controllers\Business;

use App\Http\Resources\ProductResource;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt'])->except(['index','show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProductResource::collection(Product::all()) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'weight' => 'required|numeric',
            'price' => 'required|numeric',
            'category_id' => 'required|exists:categories,id',
        ]);

        return new ProductResource(Product::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ProductResource(Product::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string',
            'description' => 'string',
            'weight' => 'numeric',
            'price' => 'numeric',
            'category_id' => 'exists:categories,id',
        ]);

        $product=Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();
        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::findOrFail($id);
        $product->delete();
        return new ProductResource($product);
    }

    public function addImage(Request $request,$id){
        $request->validate([
            'image' => 'image',
        ]);
        $product=Product::findOrFail($id);
        if($product->images->count()<3){
            $image = new Image();
            $image->image = $request->image->store('images', 'public');
            $image->product_id = $product->id;
            $image->save();
            return $image;
        }
        return response()->json(['error' => 'El producto tiene mas de 3 imagenes'], 401);
    }

    public function deleteImage($id){
        $image=Image::findOrFail($id);
        Storage::disk('public')->delete($image->image);
        $image->delete();
        return $image;
    }
}
