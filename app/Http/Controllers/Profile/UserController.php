<?php

namespace App\Http\Controllers\Profile;

use App\Http\Requests\Profile\ProfileRequest;
use App\Http\Requests\Profile\ResetPasswordRequest;
use App\Http\Requests\Profile\UserRequest;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt','verified']);
    }

    /**
     * usuario autenticado con el token que se pasa por la cabecera
     * del servicio http  (Authentication Bearer)
     * @return \Illuminate\Http\JsonResponse => se obtiene el
     */
    public function getAuthenticatedUser(){
        $user = auth()->user();
        $user->profile;
        return response()->json(['user'=>$user]);
    }

    /**
     * permite actualizar el usuario autenticado
     * @param UserRequest $request
     * @return mixed
     */
    public function update(UserRequest $request){
        $user = auth()->user();
        $user->fill($request->all());
        $profile = $user->profile;
        if($profile==null){
            $profile = new Profile();
        }
        if($request->photo){
            if($profile->photo){
                Storage::disk('public')->delete($profile->photo);
            }
            $file = $this->decodeFile($request->photo,'photos');
            $profile->photo = $file;
        }
        $profile->fill($request->all());
        $profile->user_id = $user->id;
        $profile->save();
        $user->save();
        $user->refresh();
        return response()->json(compact('user'));
    }

    /**
     * permite guardar un archivo codificado en base64
     * @param $filebase64
     * @param $path
     * @return string
     */
    private function decodeFile($filebase64,$path){
            $data = explode(',', $filebase64);
            $file = base64_decode($data[1]);
            $name = $path.'/'.time().uniqid().'.'.explode('/',explode(';',$data[0])[0])[1];
            Storage::disk('public')->put($name,$file);
            return $name;
    }

    /**
     * permite modificar la clave del usuario
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ResetPasswordRequest $request){
        $user = auth()->user();

        if(Hash::check($request->old_password, $user->password)){
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json(compact('user'));
        }
        return response()->json(['errors'=>['password'=>trans('auth.failed')]],422);
    }
}
