@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>@lang('messages.verified')</h1>
    </div>
@endsection