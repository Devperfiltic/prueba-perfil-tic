<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// reiniciar contraseña
Route::get('password/reset/{token}','AuthApi\ResetPasswordController@showResetForm')
    ->name('password.reset');

Route::post('password/reset','AuthApi\ResetPasswordController@reset')
    ->name('password.update');

Route::get('password/confirm', function () {
    return view('auth.passwords.confirmReset');
});

//verificar email
Route::get('email/verify','AuthApi\VerificationController@show')
    ->name('verification.notice');

Route::get('email/verify/{id}','AuthApi\VerificationController@verify')
    ->name('verification.verify');
